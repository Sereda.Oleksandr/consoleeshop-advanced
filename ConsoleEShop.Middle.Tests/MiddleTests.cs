using System;
using System.Collections.Generic;
using ConsoleEShop.Middle.Entities;
using ConsoleEShop.Middle.Interfaces;
using ConsoleEShop.Middle.Services;
using Xunit;

namespace ConsoleEShop.Middle.Tests
{
    public class MiddleTests
    {
        private readonly IUnitOfWork db = new UnitOfWork.UnitOfWork();
        readonly IOrderService _orderService;
        readonly IUserService _userService;
        readonly IGoodService _goodService;

        public MiddleTests()
        {
            _orderService = new OrderService(db);
            _userService = new UserService(db);
            _goodService = new GoodService(db);
        }

        [Fact]
        public void Shop_IsSingleton()
        {
            var newShopService = new GoodService(db);
            var product = new Good() { Name = "Name", Description = "Description", Price = 10.25M };
            _goodService.CreateGood(product);

            Assert.Equal(newShopService.GetGoods().Count, _goodService.GetGoods().Count);
            Assert.Same(newShopService.GetGood(product.Name), _goodService.GetGood(product.Name));
        }

        [Theory]
        [InlineData("login", "password", "FName", "LName", "somemail@gmail.com")]
        [InlineData("01ue", "123456gh", "Name", "LastName", "_any@gmail.com")]
        public void Register_WhenValidUserInfo_ThenUsersCountChanged(string login, string password, string firstName, string lastName, string email)
        {
            var initialCount = _userService.GetUsers().Count;
            var user = new User() { Login = login, Password = password, FirstName = firstName, LastName = lastName, Email = email };

            _userService.Register(user);
            var resultCount = _userService.GetUsers().Count;

            Assert.NotEqual(initialCount, resultCount);
        }

        [Theory]
        [InlineData("", "password", "FName", "LName", "somemail@gmail.com")]
        [InlineData("01ue", "", "Name", "", "any@gmail.com")]
        public void Register_WhenInvalidUserInfo_ThenThrowArgumentException(string login, string password, string firstName, string lastName, string email)
        {
            var user = new User() { Login = login, Password = password, FirstName = firstName, LastName = lastName, Email = email };

            Assert.Throws<ArgumentException>(() => _userService.Register(user));
        }

        [Theory]
        [InlineData("user1", "password")]
        [InlineData("User1Login", "123456")]
        [InlineData("admin", "111111")]
        public void LogIn_WhenNotExistingUser_ThenThrowInvalidOperationException(string login, string password)
        {
            Assert.Throws<InvalidOperationException>(() => _userService.LogIn(login, password));
        }

        [Fact]
        public void GetProducts_WhenCalled_ThenReturnAListOfProducts()
        {
            var products = _goodService.GetGoods();

            Assert.IsType<List<Good>>(products);
        }

        [Theory]
        [InlineData("product1Name")]
        [InlineData("")]
        [InlineData("pn7")]
        public void GetProductByName_WhenNotExistingProduct_ThenThrowInvalidOperationException(string name)
        {
            Assert.Throws<InvalidOperationException>(() => _goodService.GetGood(name));
        }

        [Theory]
        [InlineData(14)]
        [InlineData(117)]
        public void GetProductById_WhenNotExistingProduct_ThenThrowInvalidOperationException(int id)
        {
            Assert.Throws<InvalidOperationException>(() => _goodService.GetGood(id));
        }

        [Theory]
        [InlineData(1, new int[] { 0, 1, 3 })]
        [InlineData(3, new int[] { 3, 2 })]
        public void CreateOrder_WhenValidOrderInfo_ThenGetProductsContainsAddedProduct(int userId, int[] productIds)
        {
            var order = new Order() 
            { 
                Items = new List<Good>(),
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_goodService.GetGood(item));
            }

            _orderService.CreateOrder(order);

            Assert.Contains(order, _orderService.GetUserOrders(userId));
        }

        [Theory]
        [InlineData(3, new int[] { 1, 0, 2 })]
        public void CancelOrder_WhenPossibleToCancel_ThenOrderStatusChanged(int userId, int[] productIds)
        {
            var order = new Order()
            {
                Items = new List<Good>(),
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_goodService.GetGood(item));
            }

            _orderService.CreateOrder(order);

            _orderService.CancelOrder(_userService.GetUserInfo(userId), _orderService.GetUserOrders(userId).Find(x => x == order).Id);

            Assert.Equal(OrderStatus.CanceledByUser, order.Status);
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(27, 0)]
        public void GetUserOrders_WhenExistingUserId_ThenReturnExpectedCount(int userId, int expectedOrdersCount)
        {
            var actualOrdersCount = _orderService.GetUserOrders(userId).Count;

            Assert.Equal(expectedOrdersCount, actualOrdersCount);
        }

        [Theory]
        [InlineData(115)]
        public void GetUserOrders_WhenNotExistingUserId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _orderService.GetUserOrders(userId));
        }

        [Theory]
        [InlineData("SomeProductName", "SomeProductDescription", 34)]
        [InlineData("NewProdName", "NewProdDesc", 201)]
        public void CreateProduct_WhenValidProductInfo_ThenGetProductsContainsAddedProduct(string name, string description, decimal price)
        {
            var product = new Good() { Name = name, Description = description, Price = price };

            _goodService.CreateGood(product);

            Assert.Contains(product, _goodService.GetGoods());
        }

        [Theory]
        [InlineData("", "SomeProductDescription", 34)]
        [InlineData("NewProdName", "NewProdDesc", 0.0)]
        [InlineData("NewProdName", "NewProdDesc", -459)]
        public void CreateProduct_WhenInvalidProductInfo_ThenThrowInvalidOperationException(string name, string description, decimal price)
        {
            var product = new Good() { Name = name, Description = description, Price = price};

            Assert.Throws<InvalidOperationException>(() => _goodService.CreateGood(product));
        }


        [Theory]
        [InlineData(-10, 3)]
        public void ProceedOrder_WhenInvalidOrderId_ThenThrowArgumentException(int orderId, int userId)
        {
            var user = _userService.GetUserInfo(userId);

            Assert.Throws<ArgumentException>(() => _orderService.ProceedOrder(user, orderId));
        }

        [Theory]
        [InlineData(17, OrderStatus.Sent)]
        [InlineData(21, OrderStatus.PaymentReceived)]
        public void SetOrderStatus_WhenValidOrderId_ThenOrderStatusChanged(int userId, OrderStatus newStatus)
        {
            var order = new Order()
            {
                Items = new List<Good>(),
                UserId = userId,                
            };

            var newOrderInfo = _orderService.CreateOrder(order);
            var initialOrderStatus = newOrderInfo.Status;

            _orderService.SetOrderStatus(newOrderInfo.Id, newStatus);

            Assert.NotEqual(initialOrderStatus, newOrderInfo.Status);
            Assert.Equal(newStatus, newOrderInfo.Status);
        }

        [Theory]
        [InlineData(-1, OrderStatus.Sent)]
        [InlineData(211, OrderStatus.PaymentReceived)]
        public void SetOrderStatus_WhenInvalidOrderId_ThenThrowArgumentException(int orderId, OrderStatus newStatus)
        {
            Assert.Throws<ArgumentException>(() => _orderService.SetOrderStatus(orderId, newStatus));
        }

        [Fact]
        public void GetUsers_WhenCalled_ThenReturnAListOfUsers()
        {
            var users = _userService.GetUsers();

            Assert.IsType<List<User>>(users);
        }

        [Theory]
        [InlineData(25, "Alfred", "Ward", "Alfred_Ward44@yahoo.com")]
        [InlineData(11, "Sherman", "Kuhlman", "Sherman_Kuhlman44@hotmail.com")]
        public void GetUserInfo_WhenExistingUserId_ThenReturnExpectedResult(int userId, string firstName, string lastName, string email)
        {
            var user = _userService.GetUserInfo(userId);

            Assert.Equal(firstName, user.FirstName);
            Assert.Equal(lastName, user.LastName);
            Assert.Equal(email, user.Email);
        }

        [Theory]
        [InlineData(-25)]
        [InlineData(150)]
        public void GetUserInfo_WhenNotExistingUserId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _userService.GetUserInfo(userId));
        }

        [Theory]
        [InlineData(4, "NewFName", "NewLName", "Dianna_Lynch@hotmail.com", "newLogin", "_1212 134")]
        [InlineData(17, "fName", "lName", "Beatrice.Kerluke@gmail.com", "98713Login", "_1231jbui")]
        public void UpdateUserInfo_WhenIdAndBodyAreValid_ThenUserInfoChanged(int userId, string newFirstName, string newLastName, string newEmail, string newLogin, string newPass)
        {
            var newUserInfo = new User() { FirstName = newFirstName, LastName = newLastName, Email = newEmail, Login = newLogin, Password = newPass };
            var userInfoBeforeUpdate = _userService.GetUserInfo(userId);

            var newUserInfoList = new List<string>()
            {
                newUserInfo.FirstName,
                newUserInfo.LastName,
                newUserInfo.Email,
                newUserInfo.Login,
                newUserInfo.Password
            };

            var userInfoBeforeUpdateList = new List<string>()
            {
                userInfoBeforeUpdate.FirstName,
                userInfoBeforeUpdate.LastName,
                userInfoBeforeUpdate.Email,
                userInfoBeforeUpdate.Login,
                userInfoBeforeUpdate.Password
            };

            _userService.UpdateUserInfo(userId, newUserInfo);
            var userInfoAfterUpdate = _userService.GetUserInfo(userId);

            var userInfoAfterUpdateList = new List<string>()
            {
                userInfoAfterUpdate.FirstName,
                userInfoAfterUpdate.LastName,
                userInfoAfterUpdate.Email,
                userInfoAfterUpdate.Login,
                userInfoAfterUpdate.Password
            };

            Assert.NotEqual(userInfoBeforeUpdateList, userInfoAfterUpdateList);
            Assert.Equal(newUserInfoList, userInfoAfterUpdateList);
        }
        

        [Theory]
        [InlineData(-1)]
        [InlineData(125)]
        public void UpdateUserInfo_WhenIdIsInvalid_ThenThrowArgumentExcetion(int userId)
        {
            var newUserInfo = new User() { FirstName = "newFirstName", LastName = "newLastName", Email = "newEmail@gmail.com", Login = "newLogin", Password = "newPass" };

            Assert.Throws<ArgumentException>(() => _userService.UpdateUserInfo(userId, newUserInfo));
        }

        [Theory]
        [InlineData("Product9Name", "NewProdName", "NewProdDescription", 23.50)]
        [InlineData("Product2Name", "Prod1Name", "101", 0.50)]
        public void UpdateProductInfo_WhenBodyIsValid_ThenProductInfoChanged(string productName, string newProductName, string newProductDescription, decimal newProdusctPrice)
        {
            var newProductInfo = new Good() { Name = newProductName, Description = newProductDescription, Price = newProdusctPrice };
            var productInfoBeforeUpdate = _goodService.GetGood(productName);

            var newProductInfoList = new List<string>()
            {
                newProductInfo.Name,
                newProductInfo.Description,
                newProductInfo.Price.ToString(),
            };

            var productInfoBeforeUpdateList = new List<string>()
            {
                productInfoBeforeUpdate.Name,
                productInfoBeforeUpdate.Description,
                productInfoBeforeUpdate.Price.ToString(),
            };

            _goodService.UpdateGoodInfo(productInfoBeforeUpdate.Id, newProductInfo);
            var productInfoAfterUpdate = _goodService.GetGood(productInfoBeforeUpdate.Id);

            var productInfoAfterUpdateList = new List<string>()
            {
                productInfoAfterUpdate.Name,
                productInfoAfterUpdate.Description,
                productInfoAfterUpdate.Price.ToString(),
            };
            
            Assert.Equal(newProductInfoList, productInfoAfterUpdateList);
        }

        [Theory]
        [InlineData("Product1Name", "", "NewProdDescription", 23.50)]
        [InlineData("Product10Name", "Prod10Name", "", 0.50)]
        [InlineData("Product7Name", "7Name", "NewProdDescription", -100.0)]
        [InlineData("Product4Name", "ProdName", "NewProdDescription", 0.0)]
        public void UpdateProductInfo_WhenBodyIsInvalid_ThenThrowInvalidOperationException(string productName, string newProductName, string newProductDescription, decimal newProdusctPrice)
        {
            var newProductInfo = new Good() { Name = newProductName, Description = newProductDescription, Price = newProdusctPrice };
            var searchedProduct = _goodService.GetGood(productName);
            
            Assert.Throws<InvalidOperationException>(() => _goodService.UpdateGoodInfo(searchedProduct.Id, newProductInfo));
        }
    }
}