﻿using System.Collections.Generic;
using ConsoleEShop.Advanced.Entities;

namespace ConsoleEShop.Advanced.Db
{
    public class DbContext
    {
        public virtual List<User> Users { get; set; } = DataSource.Users;
        public virtual List<Order> Orders { get; set; } = DataSource.Orders;
        public virtual List<Good> Goods { get; set; } = DataSource.Goods;
    }
}