﻿using System;

namespace ConsoleEShop.Advanced
{
    class Program
    {
        static void Main(string[] args)
        {
            while (MenuProvider.toContinue)
                MenuProvider.ProvideMenu();
        }
    }
}