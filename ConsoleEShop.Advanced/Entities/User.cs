﻿using System;

namespace ConsoleEShop.Advanced.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Roles Role { get; set; }
        public DateTime RegisteredAt { get; set; }
        public decimal Balance { get; set; }

    }
}