﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleEShop.Advanced.Db;
using ConsoleEShop.Advanced.Entities;
using ConsoleEShop.Advanced.Interfaces;
using ConsoleEShop.Advanced.Repositories;
using Moq;

namespace ConsoleEShop.Advanced.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Mock<DbContext> _contextMock;
        private OrderRepository _orderRepository;
        private GoodRepository _goodRepository;
        private UserRepository _userRepository;
        
        private static UnitOfWork instance;
        private static readonly object locker = new object();
        public static UnitOfWork Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? (instance = new UnitOfWork());
                }
            }
        }
        
        public UnitOfWork()
        {
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(context => context.Goods).Returns(DataSource.Goods);
            _contextMock.Setup(context => context.Orders).Returns(DataSource.Orders);
            _contextMock.Setup(context => context.Users).Returns(DataSource.Users);
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_contextMock.Object);
                return _userRepository;
            }
        }


        public IRepository<Order> Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_contextMock.Object);
                return _orderRepository;
            }
        }

        public IRepository<Good> Goods
        {
            get
            {
                if (_goodRepository == null)
                    _goodRepository = new GoodRepository(_contextMock.Object);
                return _goodRepository;
            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

    }
}