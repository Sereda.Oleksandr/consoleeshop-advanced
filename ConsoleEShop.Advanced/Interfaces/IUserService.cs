﻿using System.Collections.Generic;
using ConsoleEShop.Advanced.Entities;

namespace ConsoleEShop.Advanced.Interfaces
{
    public interface IUserService
    {
        public User LogIn(string login, string password);
        public User Register(User newUserInfo);
        public void UpdateUserInfo(int userId, User newUserInfo);
        public User GetUserInfo(int userId);
        public List<User> GetUsers();

    }
}