﻿using System.Collections.Generic;
using ConsoleEShop.Advanced.Entities;

namespace ConsoleEShop.Advanced.Interfaces
{
    public interface IOrderService
    {
        public Order CreateOrder(Order newOrderInfo);
        public void ProceedOrder(User currentUser, int orderId);
        public void CancelOrder(User currentUser, int orderId);
        public List<Order> GetUserOrders(int userId);
        public void SetOrderStatus(int orderId, OrderStatus newStatus);
    }
}