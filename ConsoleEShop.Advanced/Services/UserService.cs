﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Advanced.Entities;
using ConsoleEShop.Advanced.Interfaces;

namespace ConsoleEShop.Advanced.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork database;
        public UserService(IUnitOfWork database)
        {
            this.database = database;
        }
        public User LogIn(string login, string password)
        {
            var currentUser = database.Users.Get().FirstOrDefault(x => x.Login == login && x.Password == password);

            if (currentUser == null) throw new InvalidOperationException("User with such login and password not found.");

            return currentUser;
        }

        public User Register(User newUserInfo)
        {
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new ArgumentException("Login/Password cannot be an empty string.");

            newUserInfo.Id = database.Users.Get().Count;
            database.Users.Create(newUserInfo);

            return newUserInfo;
        }

        public void UpdateUserInfo(int userId, User newUserInfo)
        {
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new InvalidOperationException("Login/password cannot be an empty string.");

            database.Users.Update(newUserInfo, userId);

        }

        public User GetUserInfo(int userId)
        {
            return database.Users.Get(userId);
        }

        public List<User> GetUsers()
        {
            return database.Users.Get();
        }
    }
}