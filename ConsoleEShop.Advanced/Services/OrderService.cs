﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Advanced.Entities;
using ConsoleEShop.Advanced.Interfaces;

namespace ConsoleEShop.Advanced.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork database;
        public OrderService(IUnitOfWork database)
        {
            this.database = database;
        }
        
        public Order CreateOrder(Order newOrderInfo)
        {
            if (database.Users.Get().FirstOrDefault(x => x.Id == newOrderInfo.UserId) == null) throw new InvalidOperationException("User with Id specified in the field UserId does not exist.");

            newOrderInfo.Id = database.Orders.Get().Count;
            newOrderInfo.Status = OrderStatus.New;
            newOrderInfo.CreatedAt = DateTime.Now;

            database.Orders.Create(newOrderInfo);

            return newOrderInfo;
        }

        public void ProceedOrder(User currentUser, int orderId)
        {
            var order = database.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new ArgumentException("Order with such id not found.", "orderId");

            if (order.Status == OrderStatus.PaymentReceived || order.Status == OrderStatus.Sent) throw new InvalidOperationException("This order is already in progress.");

            if (order.Status == OrderStatus.CanceledByAdmin) throw new InvalidOperationException("This order has been canceled by administrator.");
            
        }

        public void CancelOrder(User currentUser, int orderId)
        {
            var order = database.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new InvalidOperationException("Order with such id not found.");

            if (order.Status == OrderStatus.Received) throw new InvalidOperationException("Unable to cancel order because order status is 'Received'.");

            if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser) throw new InvalidOperationException("Order is already canceled.");

            if (order.Status != OrderStatus.New)
            {
                currentUser.Balance += order.Total;
            }

            order.Status = OrderStatus.CanceledByUser;
        }

        public List<Order> GetUserOrders(int userId)
        {
            if (database.Users.Get().FirstOrDefault(x => x.Id == userId) == null) throw new ArgumentException("User with such Id does not exist.", "userId");

            return database.Orders.Get().Where(x => x.UserId == userId).ToList();
        }


        public void SetOrderStatus(int orderId, OrderStatus newStatus)
        {
            var searchedOrder = database.Orders.Get().FirstOrDefault(x => x.Id == orderId);

            if (searchedOrder == null) throw new ArgumentException("Order with such Id not found.", "orderId");            

            searchedOrder.Status = newStatus;

            if (newStatus == OrderStatus.Completed)
                searchedOrder.FinishedAt = DateTime.Now;

        }
    }
}