﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Advanced.Entities;
using ConsoleEShop.Advanced.Interfaces;

namespace ConsoleEShop.Advanced.Services
{
    public class GoodService : IGoodService
    {
        private readonly IUnitOfWork database;
        public GoodService(IUnitOfWork database)
        {
            this.database = database;
        }
        
        public Good GetGood(int id)
        {
            var searchedProduct = database.Goods.Get(id);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such Id not found.");

            return searchedProduct;
        }

        
        public Good CreateGood(Good product)
        {
            if (string.IsNullOrWhiteSpace(product.Name)) throw new InvalidOperationException("Product name cannot be an empty string.");
            if (product.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero value.");

            product.Id = database.Goods.Get().Count;

            database.Goods.Create(product);

            return product;
        }

        public List<Good> GetGoods()
        {
            return database.Goods.Get();
        }

        public Good GetGood(string name)
        {
            var searchedProduct = database.Goods.Get().FirstOrDefault(x => x.Name == name);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such name not found.");

            return searchedProduct;
        }

        public void UpdateGoodInfo(int productId, Good newProductInfo)
        {
            if (string.IsNullOrWhiteSpace(newProductInfo.Name) || string.IsNullOrWhiteSpace(newProductInfo.Description))
                throw new InvalidOperationException("Product name/description cannot be an empty string.");
            if (newProductInfo.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero number.");
            database.Goods.Update(newProductInfo, productId);
        }
    }
}