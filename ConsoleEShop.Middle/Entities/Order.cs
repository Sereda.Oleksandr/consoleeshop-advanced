﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop.Middle.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public decimal Total { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int UserId { get; set; }
        public List<Good> Items { get; set; }
    }
}