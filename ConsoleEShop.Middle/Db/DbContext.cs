﻿using System.Collections.Generic;
using ConsoleEShop.Middle.Entities;

namespace ConsoleEShop.Middle.Db
{
    public class DbContext
    {
        public List<User> Users { get; set; } = DataSource.Users;
        public List<Order> Orders { get; set; } = DataSource.Orders;
        public List<Good> Goods { get; set; } = DataSource.Goods;
    }
}