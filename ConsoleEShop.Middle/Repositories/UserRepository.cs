﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Middle.Db;
using ConsoleEShop.Middle.Entities;
using ConsoleEShop.Middle.Interfaces;

namespace ConsoleEShop.Middle.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly DbContext context;

        public UserRepository(DbContext context)
        {
            this.context = context;
        }
        
        public List<User> Get()
        {
            return context.Users;
        }

        public User Get(int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            return searchedUser;
        }

        public void Create(User entity)
        {
            context.Users.Add(entity);
        }

        public void Update(User entity, int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            searchedUser.Login = entity.Login;
            searchedUser.Password = entity.Password;
            searchedUser.FirstName = entity.FirstName;
            searchedUser.LastName = entity.LastName;
            searchedUser.Email = entity.Email;
        }

        public void Delete(User entity)
        {
            context.Users.Remove(entity);
        }

        public void Delete(int id)
        {
            var itemToDelete = context.Users.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("User with such Id does not exist.");

            context.Users.Remove(itemToDelete);
        }

    }
}