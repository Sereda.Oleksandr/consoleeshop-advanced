﻿using System;

namespace ConsoleEShop.Middle
{
    class Program
    {
        static void Main(string[] args)
        {
            while (MenuProvider.toContinue)
                MenuProvider.ProvideMenu();
        }
    }
}