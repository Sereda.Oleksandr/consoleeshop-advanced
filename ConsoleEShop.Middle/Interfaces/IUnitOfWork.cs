﻿using System.Threading.Tasks;
using ConsoleEShop.Middle.Entities;

namespace ConsoleEShop.Middle.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Good> Goods { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }

}