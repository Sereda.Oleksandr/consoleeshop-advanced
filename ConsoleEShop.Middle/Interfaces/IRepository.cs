﻿using System.Collections.Generic;

namespace ConsoleEShop.Middle.Interfaces
{
    public interface IRepository<TEntity>
    {
        List<TEntity> Get();

        TEntity Get(int id);

        void Create(TEntity entity);

        void Update(TEntity entity, int id);

        void Delete(TEntity entity);

        void Delete(int id);

    }
}