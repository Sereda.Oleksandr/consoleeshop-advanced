﻿using System.Collections.Generic;
using ConsoleEShop.Middle.Entities;

namespace ConsoleEShop.Middle.Interfaces
{
    public interface IGoodService
    {
        public List<Good> GetGoods();
        public Good GetGood(string name);
        public Good GetGood(int id);
        public Good CreateGood(Good product);
        public void UpdateGoodInfo(int productId, Good newProductInfo);
    }
}