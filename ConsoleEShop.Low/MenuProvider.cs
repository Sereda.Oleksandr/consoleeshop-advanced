﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Low.Entities;
using ConsoleEShop.Low.Interfaces;
using ConsoleEShop.Low.Services;

namespace ConsoleEShop.Low
{
    public class MenuProvider
    {
        internal static bool toContinue = true;
        private static int actionChoiceInput;
        private static IUnitOfWork db = new UnitOfWork.UnitOfWork();
        private static IOrderService orderService = new OrderService(db);
        private static IUserService userService = new UserService(db);
        private static IGoodService goodService = new GoodService(db);

        private static string menuOptionsForGuest = "Choose action:\n" +
                                                    "0 - Exit \n"
                                                    + "1 - Log In \n"
                                                    + "2 - Register \n"
                                                    + "3 - View products \n"
                                                    + "4 - Get product by name \n";

        private static string menuOptionsforUser = "Choose action:\n" +
                                                   " 0 - Exit \n"
                                                   + " 1 - View products \n"
                                                   + " 2 - Get product by name \n"
                                                   + " 3 - Create order \n"
                                                   + " 4 - Proceed order \n"
                                                   + " 5 - Cancel order \n"
                                                   + " 6 - View orders history \n"
                                                   + " 7 - Set 'Received' order status \n"
                                                   + " 8 - View profile information \n"
                                                   + " 9 - Update profile information \n"
                                                   + "10 - Log Out \n";

        private static string menuOptionsforAdmin = "Choose action:\n" +
                                                    " 0 - Exit \n"
                                                    + " 1 - View products \n"
                                                    + " 2 - Get product by name \n"
                                                    + " 3 - Create order \n"
                                                    + " 4 - Proceed order \n"
                                                    + " 5 - View all users' info \n"
                                                    + " 6 - View user's info by Id \n"
                                                    + " 7 - Update user's info by Id \n"
                                                    + " 8 - Add product \n"
                                                    + " 9 - Update product info \n"
                                                    + "10 - Set order status \n"
                                                    + "11 - Log Out \n";

        private static Dictionary<Roles, string> UserRolesWithOptions = new Dictionary<Roles, string>()
        {
            {Roles.Guest, menuOptionsForGuest},
            {Roles.User, menuOptionsforUser},
            {Roles.Admin, menuOptionsforAdmin}
        };

        private static Action[] menuActionsForGuest = new Action[]
            {Exit, LogIn, Register, GetProducts, GetProductByName};

        private static Action[] menuActionsForUser = new Action[]
        {
            Exit, GetProducts, GetProductByName, CreateOrder, ProceedOrder, CancelOrder, GetOrderHistory,
            SetReceivedStatus, ViewUserInfo, UpdateUserInfo, LogOut
        };

        private static Action[] menuActionsForAdmin = new Action[]
        {
            Exit, GetProducts, GetProductByName, CreateOrder, ProceedOrder, ViewAllUsersInfo, ViewUserInfoById,
            UpdateUserInfoById, CreateProduct, UpdateProductInfo, SetOrderStatus, LogOut
        };

        private static Dictionary<Roles, Action[]> UserRolesWithActions = new Dictionary<Roles, Action[]>()
        {
            {Roles.Guest, menuActionsForGuest},
            {Roles.User, menuActionsForUser},
            {Roles.Admin, menuActionsForAdmin},
        };

        private static User currentUser;
        private static Roles currentUserRole = Roles.Guest;

        static MenuProvider()
        {
        }

        public static void ProvideMenu()
        {
            var menuForCurrentUser = UserRolesWithOptions[currentUserRole];
            var optionsForCurrentUser = UserRolesWithActions[currentUserRole];
            Console.WriteLine(menuForCurrentUser);

            try
            {
                Console.Write("Your input: ");
                var actionChoiceInputString = Console.ReadLine();
                actionChoiceInput = Convert.ToInt32(actionChoiceInputString);
                optionsForCurrentUser[actionChoiceInput]();
            }
            catch (FormatException)
            {
                Console.WriteLine("User input is not a number");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Input error. Enter number from 0 to {optionsForCurrentUser.Length}");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

            Console.WriteLine();
        }

        private static void Exit()
        {
            toContinue = false;
        }

        private static void LogIn()
        {
            try
            {
                Console.Write("\nLogin: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nPassword: ");
                var passwordInput = Console.ReadLine();

                var user = userService.LogIn(loginInput, passwordInput);

                Console.WriteLine($"\n Hello, {user.FirstName} {user.LastName}!");

                currentUser = user;
                currentUserRole = user.Role;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void Register()
        {
            try
            {
                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUser = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                    Role = Roles.User,
                    RegisteredAt = DateTime.Now
                };

                var user = userService.Register(newUser);

                Console.WriteLine($"\n Welcome, {user.FirstName} {user.LastName}! \nYour profile successfully created.");

                currentUser = user;
                currentUserRole = user.Role;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void GetProducts()
        {
            try
            {
                Console.WriteLine("\nProducts: \n");

                var productsList = goodService.GetGoods();

                foreach (var item in productsList)
                {
                    Console.WriteLine(
                        $"Name: {item.Name} \nDescription: {item.Description}  \nPrice: {item.Price}");
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void GetProductByName()
        {
            try
            {
                Console.Write("\nEnter product name: ");
                var input = Console.ReadLine();

                var result = goodService.GetGood(input);

                Console.WriteLine("\nProduct found:");
                Console.WriteLine(
                    $"Name: {result.Name} \nDescription: {result.Description} \nPrice: {result.Price}");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void CreateOrder()
        {
            try
            {
                var input = string.Empty;
                var goodsList = new List<Good>();

                while (true)
                {
                    Console.WriteLine("Enter item id to add to your order or X to continue: ");
                    input = Console.ReadLine();

                    if (input.ToUpper() == "X")
                        break;

                    var productId = Convert.ToInt32(input);
                    var searchedProduct = goodService.GetGoods().FirstOrDefault(x => x.Id == productId);

                    if (searchedProduct == null)
                    {
                        Console.WriteLine("Product with such Id not found.");
                        continue;
                    }

                    goodsList.Add(searchedProduct);
                    Console.WriteLine($"{searchedProduct.Name} (Id: {searchedProduct.Id}) added to your order.");
                }

                decimal total = 0;

                foreach (var item in goodsList)
                {
                    total += item.Price;
                }

                var order = new Order()
                {
                    Items = goodsList, Total = total, CreatedAt = DateTime.Now, Status = OrderStatus.New,
                    UserId = currentUser.Id
                };
                var createdOrder = orderService.CreateOrder(order);

                Console.WriteLine("Order successfuly created. Info:");
                Console.WriteLine("Items:");

                foreach (var item in createdOrder.Items)
                {
                    Console.WriteLine($"Id: {item.Id} Name: {item.Name} Price: {item.Price}");
                }

                Console.WriteLine($"\n Total : {createdOrder.Total}");
                Console.WriteLine($"To proceed this order use Id: {createdOrder.Id}");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void ProceedOrder()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                orderService.ProceedOrder(currentUser, orderId);

                Console.WriteLine($"Order successfully proceeded. To check order status use Id : {orderId}");
                Console.WriteLine($"Your current balance: {currentUser.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void CancelOrder()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                orderService.CancelOrder(currentUser, orderId);

                Console.WriteLine($"Order #{orderId} successfully canceled.");
                Console.WriteLine($"Your current balance: {currentUser.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }


        private static void GetOrderHistory()
        {
            try
            {
                Console.Write("Orders: \n");

                var userOrders = orderService.GetUserOrders(currentUser.Id);

                foreach (var element in userOrders)
                {
                    Console.WriteLine($"Order Id: {element.Id}");
                    Console.WriteLine($"Order Status: {element.Status}");
                    Console.WriteLine($"Items:");
                    foreach (var item in element.Items)
                    {
                        Console.WriteLine($"Product Id: {item.Id} Name: {item.Name}");
                    }

                    Console.WriteLine($"\n Total: {element.Total}");
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void SetReceivedStatus()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                orderService.SetOrderStatus(orderId, OrderStatus.Received);

                Console.WriteLine($"Order #{orderId} status successfuly set to 'Received'.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void ViewUserInfo()
        {
            try
            {
                Console.WriteLine($"User Id: {currentUser.Id}");
                Console.WriteLine($"First Name: {currentUser.FirstName}");
                Console.WriteLine($"Last Name: {currentUser.LastName}");
                Console.WriteLine($"Email: {currentUser.Email}");
                Console.WriteLine($"Registered: {currentUser.RegisteredAt}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void ViewUserInfoById()
        {
            try
            {
                Console.Write("Enter User Id:");
                var userId = Convert.ToInt32(Console.ReadLine());

                var user = userService.GetUserInfo(userId);

                Console.WriteLine($"User Id: {user.Id}");
                Console.WriteLine($"First Name: {user.FirstName}");
                Console.WriteLine($"Last Name: {user.LastName}");
                Console.WriteLine($"Email: {user.Email}");
                Console.WriteLine($"Registered: {user.RegisteredAt}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void ViewAllUsersInfo()
        {
            try
            {
                var users = userService.GetUsers();

                foreach (var element in users)
                {
                    Console.WriteLine($"User Id: {element.Id}");
                    Console.WriteLine($"First Name: {element.FirstName}");
                    Console.WriteLine($"Last Name: {element.LastName}");
                    Console.WriteLine($"Email: {element.Email}");
                    Console.WriteLine($"Registered: {element.RegisteredAt}");
                    Console.WriteLine();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void UpdateUserInfo()
        {
            try
            {
                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUserInfo = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                };

                userService.UpdateUserInfo(currentUser.Id, newUserInfo);

                Console.WriteLine($"Your profile successfuly updted.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void UpdateUserInfoById()
        {
            try
            {
                Console.Write("Enter User Id:");
                var userId = Convert.ToInt32(Console.ReadLine());

                var user = userService.GetUserInfo(userId);

                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUserInfo = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                };

                userService.UpdateUserInfo(userId, newUserInfo);

                Console.WriteLine($"User's profile (Id: {user.Id}) successfuly updated.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void CreateProduct()
        {
            try
            {
                Console.Write("\nEnter Product Name: ");
                var nameInput = Console.ReadLine();

                Console.Write("\nEnter Product Description: ");
                var descriptionInput = Console.ReadLine();

                Console.Write("\nEnter Product Price: ");
                var priceInput = Convert.ToDecimal(Console.ReadLine());

                var newProduct = new Good()
                {
                    Name = nameInput,
                    Description = descriptionInput,
                    Price = priceInput
                };

                var addedProduct = goodService.CreateGood(newProduct);

                Console.WriteLine($"Product successfuly created. Id: {addedProduct.Id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void UpdateProductInfo()
        {
            try
            {
                Console.Write("\nEnter Product Id: ");
                var idInput = Convert.ToInt32(Console.ReadLine());

                var searchedGood = goodService.GetGood(idInput);

                Console.Write("\nEnter Product Name: ");
                var nameInput = Console.ReadLine();

                Console.Write("\nEnter Product Description: ");
                var descriptionInput = Console.ReadLine();

                Console.Write("\nEnter Product Price: ");
                var priceInput = Convert.ToDecimal(Console.ReadLine());

                var newProductInfo = new Good()
                {
                    Name = nameInput,
                    Description = descriptionInput,
                    Price = priceInput
                };

                goodService.UpdateGoodInfo(searchedGood.Id, newProductInfo);

                Console.WriteLine($"Product #{searchedGood.Id} successfully updated.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        private static void SetOrderStatus()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter order Status:");
                var orderStatus = Convert.ToInt32(Console.ReadLine());

                if ((OrderStatus) orderStatus == OrderStatus.CanceledByUser ||
                    (OrderStatus) orderStatus == OrderStatus.Received)
                {
                    Console.WriteLine("'Canceled by user' and 'Received' order status cannot be set by administrator.");
                    return;
                }

                orderService.SetOrderStatus(orderId, (OrderStatus) orderStatus);

                Console.WriteLine($"Order #{orderId} status successfully set to '{(OrderStatus) orderStatus}'.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }

        private static void LogOut()
        {
            try
            {
                currentUserRole = Roles.Guest;
                currentUser = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
    }
}