﻿using System.Collections.Generic;
using ConsoleEShop.Low.Entities;

namespace ConsoleEShop.Low.Interfaces
{
    public interface IUserService
    {
        public User LogIn(string login, string password);
        public User Register(User newUserInfo);
        public void UpdateUserInfo(int userId, User newUserInfo);
        public User GetUserInfo(int userId);
        public List<User> GetUsers();

    }
}