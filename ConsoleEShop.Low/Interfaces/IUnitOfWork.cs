﻿using System.Threading.Tasks;
using ConsoleEShop.Low.Entities;

namespace ConsoleEShop.Low.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Good> Goods { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }

}