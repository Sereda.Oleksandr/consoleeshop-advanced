﻿using System;
using System.Threading.Tasks;
using ConsoleEShop.Low.Db;
using ConsoleEShop.Low.Entities;
using ConsoleEShop.Low.Interfaces;
using ConsoleEShop.Low.Repositories;

namespace ConsoleEShop.Low.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        private OrderRepository _orderRepository;
        private GoodRepository _goodRepository;
        private UserRepository _userRepository;

        public UnitOfWork()
        {
            _context = new DbContext();
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }


        public IRepository<Order> Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_context);
                return _orderRepository;
            }
        }

        public IRepository<Good> Goods
        {
            get
            {
                if (_goodRepository == null)
                    _goodRepository = new GoodRepository(_context);
                return _goodRepository;
            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

    }
}