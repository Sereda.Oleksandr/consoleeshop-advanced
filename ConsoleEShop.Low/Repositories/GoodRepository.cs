﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Low.Db;
using ConsoleEShop.Low.Entities;
using ConsoleEShop.Low.Interfaces;

namespace ConsoleEShop.Low.Repositories
{
    public class GoodRepository : IRepository<Good>
    {
        private readonly DbContext context;

        public GoodRepository(DbContext context)
        {
            this.context = context;
        }

        public void Delete(Good entity)
        {
            context.Goods.Remove(entity);
        }

        public void Delete(int id)
        {
            var itemToDelete = context.Goods.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("Product with such Id does not exist.");

            context.Goods.Remove(itemToDelete);
        }

        public List<Good> Get()
        {
            return context.Goods;
        }

        public Good Get(int id)
        {
            return context.Goods.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Good entity)
        {
            context.Goods.Add(entity);
        }

        public void Update(Good entity, int id)
        {
            var searchedProduct = context.Goods.FirstOrDefault(x => x.Id == id);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such Id not found.");

            searchedProduct.Name = entity.Name;
            searchedProduct.Description = entity.Description;
            searchedProduct.Price = entity.Price;
        }

    }
}