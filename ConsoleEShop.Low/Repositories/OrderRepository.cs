﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.Low.Db;
using ConsoleEShop.Low.Entities;
using ConsoleEShop.Low.Interfaces;

namespace ConsoleEShop.Low.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        private readonly DbContext context;

        public OrderRepository(DbContext context)
        {
            this.context = context;
        }

        public void Delete(Order entity)
        {
            context.Orders.Remove(entity);
        }
        
        
        public void Delete(int id)
        {
            var itemToDelete = context.Orders.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("Order with such Id does not exist.");

            context.Orders.Remove(itemToDelete);
        }

        public List<Order> Get()
        {
            return context.Orders;
        }

        public Order Get(int id)
        {
            return context.Orders.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Order entity)
        {
            context.Orders.Add(entity);
        }

        public void Update(Order entity, int id)
        {
            var searchedOrder = context.Orders.FirstOrDefault(x => x.Id == id);

            if (searchedOrder == null) throw new InvalidOperationException("Order with such Id not found.");

            searchedOrder.Items = entity.Items;
            searchedOrder.Status = entity.Status;
            searchedOrder.Total = entity.Total;
            searchedOrder.UserId = entity.UserId;               
        }

    }
}