﻿using System.Collections.Generic;
using ConsoleEShop.Low.Entities;

namespace ConsoleEShop.Low.Db
{
    public class DbContext
    {
        public List<User> Users { get; set; } = DataSource.Users;
        public List<Order> Orders { get; set; } = DataSource.Orders;
        public List<Good> Goods { get; set; } = DataSource.Goods;
    }
}