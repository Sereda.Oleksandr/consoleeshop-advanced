﻿namespace ConsoleEShop.Low.Entities
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        PaymentReceived,
        Sent,
        Received,
        Completed
    }

}